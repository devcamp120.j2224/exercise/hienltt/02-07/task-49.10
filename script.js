$(document).ready(function(){
    /*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */
    //biến toàn cục chứa thông tin đơn hàng
    var gDataOrders = [];
    var gId = "";

    var gORDER_COLS = ['orderId', 'kichCo', 'loaiPizza', 'idLoaiNuocUong', 'thanhTien' , 'hoTen', 'soDienThoai', 'trangThai', 'action'];
    const gCOL_ORDER_ID = 0;
    const gCOL_COMBO_SIZE = 1;
    const gCOL_PIZZA_TYPE = 2;
    const gCOL_DRINK = 3;
    const gCOL_TOTAL_AMOUNT = 4;
    const gCOL_FULLNAME = 5;
    const gCOL_PHONE_NUMBER = 6;
    const gCOL_STATUS = 7;
    const gCOL_ACTION = 8;

    var gOrderTable = $('#table-order').DataTable({
        searching: false,
        ordering: false,
        columns: [
            {data: gORDER_COLS[gCOL_ORDER_ID]},
            {data: gORDER_COLS[gCOL_COMBO_SIZE]},
            {data: gORDER_COLS[gCOL_PIZZA_TYPE]},
            {data: gORDER_COLS[gCOL_DRINK]},
            {data: gORDER_COLS[gCOL_TOTAL_AMOUNT]},
            {data: gORDER_COLS[gCOL_FULLNAME]},
            {data: gORDER_COLS[gCOL_PHONE_NUMBER]},
            {data: gORDER_COLS[gCOL_STATUS]},
            {data: gORDER_COLS[gCOL_ACTION]},
        ],
        columnDefs: [
            {
                targets: gCOL_TOTAL_AMOUNT,
                render: function(data){
                    return data + " VNĐ";
                }
            },
            {   
                targets: gCOL_ACTION,
                defaultContent: `<button class="btn btn-warning form-control btn-detail">Chi tiết</button>
                                <button class="btn btn-danger form-control btn-delete">Xoá</button> `                             
            },
            // {
            //     targets: gCOL_PIZZA_TYPE,
            //     render: function(data){
            //         var vTypePizzaName = renderToTypePizzaNameByTypePizzaValue(data);
            //         return vTypePizzaName;
            //     }
            // },
            {
                targets: gCOL_STATUS,
                render: function(data){
                    var vStatusText = renderToStatusTextByStatusValue(data);
                    return vStatusText;
                }
            },
        ]
    });
    // mảng Danh sách Loại Pizza
    var gPizzaTypeArr = [
        {
          typeValue: "Seafood",
          typeText: "Hải Sản"
        },
        {
            typeValue: "Hawaii",
            typeText: "Hawaii"
        },
        {
            typeValue: "Bacon",
            typeText: "Thịt hun khói"
        }
    ];
    // mảng Danh sách Trạng thái
    var gStatusArr = [
        {
          typeValue: "open",
          typeText: "Open"
        },
        {
            typeValue: "cancel",
            typeText: "Đã hủy"
        },
        {
            typeValue: "confirmed",
            typeText: "Đã xác nhận"
        }
    ];
    // mảng Thông tin kích cỡ các combo Pizza
    var gThongTinCombo = [
        {   
            kichCo: "S",
            duongKinh: "20",
            suon: "2",
            salad: "200",
            drinkNumber: "2",
            gia: "150000"
        },
        {   
            kichCo: "M",
            duongKinh: "25",
            suon: "4",
            salad: "300",
            drinkNumber: "3",
            gia: "200000"
        },
        {   
            kichCo: "L",
            duongKinh: "30",
            suon: "8",
            salad: "500",
            drinkNumber: "4",
            gia: "250000"
        }
    ];

    /*** REGION 2 - Vùng gán / thực thi hàm xử lý sự kiện cho các elements */
    onPageLoading();
    loadDataToSelectTypePizza(gPizzaTypeArr);
    loadDataToSelectStatus(gStatusArr);
    getDrinkListAjax();
    // Gán event handler cho sự kiện click nút Search
    $("#btn-search").on("click",function(){
        onBtnSearchClick();
    })
    // Gán event handler cho nút Thêm
    $('#btn-add').on('click', function(){
        onBtnAddClick();
    });
    // gán sự kiện change cho select combo
    $('#input-combo-modal').on('change', function(){
        onBtnChangeComboSelect()
    })
    // Gán event handler cho nút Thêm trên modal Add
    $('#btn-add-modal').on('click', function(){
        onBtnModalAddClick();
    });
    // Gán event handler cho nuuts Chi tiết trong table
    $('#table-order').on('click', '.btn-detail', function(){
        onBtnDetailClick(this);
    });
    // Gán event handler cho nút Xác nhận đơn hàng trên modal update status
    $('#btn-modal-confirm-order').on('click', function(){
        onBtnModalConfirmOrderClick();
    });
    // Gán event handler cho nút Huỷ đơn hàng trên modal update status
    $('#btn-modal-cancel-order').on('click', function(){
        onBtnModalCanCalOrderClick();
    });
    // Gán sự kiện change cho input ID voucher
    $('#input-idvoucher-modal').on('change', function(){
        onChangeIdVoucher();
    });
    // Gán event handler cho nút Xoá trong table
    $('#table-order').on('click', '.btn-delete', function(){
        onBtnDeleteClick(this);
    });

    /*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */
    // Hàm thực thi khi load trang
    function onPageLoading(){
        "use strict";
        // gọi API lấy tất cả order
        $.ajax({
            url: "http://42.115.221.44:8080/devcamp-pizza365/orders",
            type: "GET",
            dataType: 'json',
            success: function(res){
                console.log(res);
                gDataOrders = res;
                loadDataToOrderTable(res);
            },
            error: function(error){
                alert(error.responseText);
            }
        });
    }
    // Hàm đổ dữ liệu vào filter select type pizza
    function loadDataToSelectTypePizza(paramObject){
        "use strict";
        // Thêm option cho select filter
        $('#select-type-pizza').append($('<option>', {
            value: "none",
            text: "--- Chọn loại Pizza ---"
        }));
        for(var bI = 0; bI < paramObject.length; bI++){
          $('#select-type-pizza').append($('<option>', {
            value: paramObject[bI].typeValue,
            text: paramObject[bI].typeText
          }));
        }  
        // Thêm option cho select trên modal
        $('#select-type-pizza-modal').append($('<option>', {
            value: "none",
            text: "--- Chọn loại Pizza ---"
        }));
        for(var bI = 0; bI < paramObject.length; bI++){
          $('#select-type-pizza-modal').append($('<option>', {
            value: paramObject[bI].typeValue,
            text: paramObject[bI].typeText
          }));
        }
    }
    // Hàm đổ dữ liệu vào filter select status
    function loadDataToSelectStatus(paramObject){
        "use strict";
        // Thêm option cho select filter
        $('#select-status').append($('<option>', {
            value: "0",
            text: "--- Chọn status ---"
        }));
        for(var bI = 0; bI < paramObject.length; bI++){
          $('#select-status').append($('<option>', {
            value: paramObject[bI].typeValue,
            text: paramObject[bI].typeText
          }));
        }     
        // Thêm option cho select modal
        for(var bI = 0; bI < paramObject.length; bI++){
          $('#select-status-modal').append($('<option>', {
            value: paramObject[bI].typeValue,
            text: paramObject[bI].typeText
          }));
        }
    }
    // Hàm xử lý lọc dữ liệu
    function onBtnSearchClick(){
        'use strict';
        var vStatus = $('#select-status').val();
        var vPizzaType = $('#select-type-pizza').val();
        var vDataFilter = gDataOrders.filter( function(paramOrder, index){
            return ((vStatus == "0" || vStatus.toUpperCase() === paramOrder.trangThai.toUpperCase())
            && (vPizzaType == 'none' || vPizzaType.toUpperCase() === paramOrder.loaiPizza.toUpperCase()))
        });
        // gọi hàm load dữ liệu vào bảng
        loadDataToOrderTable(vDataFilter);
    }
    //Hàm gọi API để lấy danh sách đồ uống
    function getDrinkListAjax(){
        'use strict';
        $.ajax({
            url: 'http://42.115.221.44:8080/devcamp-pizza365/drinks',
            dataType: 'json',
            type: 'GET',
            success: function(res){
                // tạo option cho select đồ uống theo dữ liệu nhận về
                loadDataToSelectDrinkModal(res);
            },
            error: function(error){
                alert(error.responseText);
            }
        });
    }
    // Hàm xử lý khi nhấn nút Thêm
    function onBtnAddClick(){
        "use strict";
        $('#add-order-modal').modal();
        clearModalAddOrder();
    }
    // Hám xử lý khi nhấn nút Thêm trên modal
    function onBtnModalAddClick(){
        "use strict";
        //Bước 0: khai báo object order để chứa thông tin đặt hàng 
        var vDataOrder = {
            kichCo: "",
            duongKinh: "",
            suon: "",
            salad: "",
            soLuongNuoc: "",  
            thanhTien: "",
            loaiPizza: "",
            idVourcher: "",
            idLoaiNuocUong: "",
            hoTen: "",
            email: "",
            soDienThoai: "",
            diaChi: "",
            loiNhan: "",
            trangThai: "",
            giamGia: "",   
        };
        //Bước 1: thu thập dữ liệu
        getDataFromFormToCreateOrder(vDataOrder);
        var vCheck = validateData(vDataOrder);
        console.log(vCheck);
        if(vCheck){
            $.ajax({
                url: "http://42.115.221.44:8080/devcamp-pizza365/orders",
                type: "POST",
                contentType: 'application/json;charset=UTF-8',
                data: JSON.stringify(vDataOrder),
                success: function(res){
                    alert('Thêm đơn hàng thành công!');
                    onPageLoading();
                    $('#add-order-modal').modal('hide');
                },
                error: function(error){
                    alert(error.responseText);
                }
            });
        }
    }

    // Hàm xử lý khi change select combo
    function onBtnChangeComboSelect(){
        "use strict";
        $('#input-idvoucher-modal').val("");
        $('#input-giam-gia-modal').val("")
        // console.log("change select");  
        var vComboValue = $("#input-combo-modal").val(); // lấy brandId từ select
        // console.log(vComboValue);
        for (var bI = 0; bI < gThongTinCombo.length; bI++){
            if (vComboValue == gThongTinCombo[bI].kichCo){
                $('#input-duong-kinh-modal').val(gThongTinCombo[bI].duongKinh);
                $('#input-suon-modal').val(gThongTinCombo[bI].suon);
                $('#input-salad-modal').val(gThongTinCombo[bI].salad);
                $('#input-soluongdrink-modal').val(gThongTinCombo[bI].drinkNumber);
                $('#input-money-modal').val(gThongTinCombo[bI].gia);
            }
            else if (vComboValue == "none"){
                $('#input-duong-kinh-modal').val("");
                $('#input-suon-modal').val("");
                $('#input-salad-modal').val("");
                $('#input-soluongdrink-modal').val("");
                $('#input-money-modal').val("");
            }
        }
    }        
    // Hàm xử lý khi nhấn vào nút Chi tiết trong table
    function onBtnDetailClick(paramBtn){
        "use strict";
        //lấy ra data user tương ứng nút được click
        var vDataCurrentRow = getDataByButton(paramBtn);
        console.log("OrderId tương ứng: " + vDataCurrentRow.orderId);
        gId = vDataCurrentRow.id;
        $.ajax({
            url: "http://42.115.221.44:8080/devcamp-pizza365/orders/" + vDataCurrentRow.orderId,
            type: 'GET',
            dataType: 'json',
            success: function(res){
                showDataOrderToModal(res);
            },
            error: function(error){
                alert(error.responseText);
            }
        });
        $('#update-status-modal').modal();        
    }
    // hàm gọi khi nhấn nút Xác nhận đơn hàng trên modal update status
    function onBtnModalConfirmOrderClick(){
        "use strict";
        console.log("Id của order là: " + gId);
        //Bước 0: khai bao biến lưu đối tượng
        var vStatusConfirm = {
          trangThai: "confirmed" //3 trang thai open, confirmed, cancel
        };
        //Bước 1: thu thập dữ liệu(ko có)
        //Bước 2: valideta (ko có)
        //Bước 3: Gọi API để update trạng thái
        if ($('#inp-status-modal-update').val() !== "confirmed"){
            $.ajax({
                url: "http://42.115.221.44:8080/devcamp-pizza365/orders/" + gId,
                type: "PUT",
                contentType: "application/json;charset=UTF-8",
                data: JSON.stringify(vStatusConfirm),
                success: function(res){
                    alert('Xác nhận đơn hàng thành công!');
                    onPageLoading();
                    $('#update-status-modal').modal('hide');
                },
                error: function(error){
                    alert(error.responseText);
                }
            }); 
        }
        else {
            alert("Đơn hàng đang ở trạng thái 'Đã xác nhận'!");
        }
                 
    }
    // hàm gọi khi nhấn nút Huỷ đơn hàng trên modal update status
    function onBtnModalCanCalOrderClick(){
        "use strict";
        console.log("Id của order là: " + gId);
        //Bước 0: khai bao biến lưu đối tượng
        var vStatusConfirm = {
          trangThai: "cancel" //3 trang thai open, confirmed, cancel
        };
        //Bước 1: thu thập dữ liệu(ko có)
        //Bước 2: valideta (ko có)
        //Bước 3: Gọi API để update trạng thái
        if ($('#inp-status-modal-update').val() !== "cancel"){
            $.ajax({
                url: "http://42.115.221.44:8080/devcamp-pizza365/orders/" + gId,
                type: "PUT",
                contentType: "application/json;charset=UTF-8",
                data: JSON.stringify(vStatusConfirm),
                success: function(res){
                    alert('Đã huỷ đơn hàng!!');
                    onPageLoading();
                    $('#update-status-modal').modal('hide');
                },
                error: function(error){
                    alert(error.responseText);
                }
            });
        }
        else {
            alert("Đơn hàng đang ở trạng thái 'Đã hủy'!");
        };
    }
    // Hàm xử lý khi change input Id voucher
    function onChangeIdVoucher(){
        "use strict";
        var vValueInpIDVoucher = $('#input-idvoucher-modal').val().trim();
        var vValueInpGiamGia = $('#input-giam-gia-modal');
        var vValueInpGia = $('#input-money-modal').val();
        console.log(vValueInpIDVoucher);
        if(vValueInpGia == ""){
            alert("Hãy chọn Combo trước!");
            $('#input-idvoucher-modal').val("");
        }
        else{
            $.ajax({
                url: "http://42.115.221.44:8080/devcamp-voucher-api/voucher_detail/" + vValueInpIDVoucher,
                type: 'GET',
                dataType: 'json',
                success: function(res){
                    console.log(res);
                    var vPhanTramDiscount = res.phanTramGiamGia;                    
                    vValueInpGiamGia.val(vValueInpGia * vPhanTramDiscount / 100);
                },
                error: function(error){
                    alert("Mã voucher " + vValueInpIDVoucher + " không tồn tại!");
                }
            })
        }
        
    }
    // Hàm xử lý khi nút Xoá được nhấn
    function onBtnDeleteClick(paramBtn){
        "use strict";
        //lấy ra data user tương ứng nút được click
        var vDataCurrentRow = getDataByButton(paramBtn);
        console.log("Id tương ứng: " + vDataCurrentRow.id);
        $.ajax({
            url: "http://42.115.221.44:8080/devcamp-pizza365/orders/" + vDataCurrentRow.id,
            type: 'DELETE',
            dataType: 'json',
            success: function(res){
                alert("Đã xoá đơn hàng thành công!");
                onPageLoading();
            },
            error: function(error){
                alert(error.responseText);
            }
        });
    }
    /*** REGION 4 - Common funtions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình*/
    // Hàm xử lý load dữ liệu order vào table
    function loadDataToOrderTable(paramCourses){
        "use strict";
        gOrderTable.clear();
        gOrderTable.rows.add(paramCourses);
        gOrderTable.draw();
    }
    // Hàm render từ type pizza value sang typy pizza text
    function renderToTypePizzaNameByTypePizzaValue(paramData) {
        "use strict";
        for (let bI = 0; bI < gPizzaTypeArr.length; bI++) {
            if (paramData.toUpperCase() == gPizzaTypeArr[bI].typeValue.toUpperCase()) {
                var vTypePizzaText = gPizzaTypeArr[bI].typeText;
                return vTypePizzaText;
            }  
        }
    }
    // Hàm render từ status value sang status text
    function renderToStatusTextByStatusValue(paramData) {
        "use strict";
        for (let bI = 0; bI < gStatusArr.length; bI++) {
            if (paramData.toUpperCase() == gStatusArr[bI].typeValue.toUpperCase()) {
                var vStatusText = gStatusArr[bI].typeText;
                return vStatusText;
            }  
        }
    }
    // Hàm xử lý tạo option cho select đồ uống trên modal
    function loadDataToSelectDrinkModal(paramResponse){
        'use strict';
        $.each(paramResponse, function(i, item){
            $('#select-drink-modal').append($('<option>', {
                text: item.tenNuocUong,
                value: item.maNuocUong
            }));
        });
    }
    // Hàm thu thập dữ liệu (bước 1 nút Tạo đơn hàng)
    function getDataFromFormToCreateOrder(paramData){
        "use strict";        
        paramData.kichCo = $('#input-combo-modal').val();
        paramData.duongKinh = $('#input-duong-kinh-modal').val();
        paramData.suon =$('#input-suon-modal').val();
        paramData.salad =$('#input-salad-modal').val();
        paramData.soLuongNuoc =$('#input-soluongdrink-modal').val();
        paramData.thanhTien =$('#input-money-modal').val();
        paramData.loaiPizza = $('#select-type-pizza-modal :selected').val();
        paramData.idVourcher = $('#input-idvoucher-modal').val().trim();
        paramData.idLoaiNuocUong = $('#select-drink-modal :selected').val();
        paramData.hoTen = $('#input-hoten-modal').val().trim();
        paramData.email = $('#input-email-modal').val().trim(); 
        paramData.soDienThoai = $('#input-phone-modal').val().trim();
        paramData.diaChi = $('#input-address-modal').val().trim();
        paramData.loiNhan = $('#input-message-modal').val().trim();
        paramData.trangThai = $('#select-status-modal :selected').val();
        paramData.giamGia = $('#input-giam-gia-modal').val();
    }
    // Hàm kiểm tra dữ liệu sau khi thu thập trên modal
    function validateData(paramObj){
        "use strict";
        var vRegexStr = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;
        var vResult = true;
        if (paramObj.kichCo == "none") {            
            alert("Hãy chọn Combo!");
            vResult = false;
        }
        else if (paramObj.loaiPizza == "none") {
            vResult = false;
            alert("Hãy chọn loại Pizza!");
        } 
        else if (paramObj.idLoaiNuocUong == "none"){
            vResult = false;
            alert("Hãy chọn đồ uống!");
        }
        else if (paramObj.hoTen == "") {
            vResult = false;
            alert("Hãy nhập họ tên!");
        }
        else if(paramObj.email == ""){
            alert("Hãy nhập email!");
            vResult = false;
        }
        else if(!vRegexStr.test(paramObj.email)){
            alert("Email không hợp lệ");
            vResult = false;
        }
        else if (paramObj.soDienThoai == "") {
            vResult = false;
            alert("Hãy nhập số điện thoại!");
        }
        else if(isNaN(paramObj.soDienThoai)){
            vResult = false;
            alert("Số điện thoại phải là số!");
        }
        else if (paramObj.diaChi == "") {
            vResult = false;
            alert("Hãy nhập địa chỉ!");
        } 
        return vResult;
    }
    // Hàm lấy ra dữ liệu của hàng chứa icon được click
    function getDataByButton(paramBtn){
        "use strict";
        var vCurrentRow = $(paramBtn).closest('tr'); 
        var vDataTable = $('#table-order').DataTable();
        var vCurrentRowData = vDataTable.row(vCurrentRow).data();
        return vCurrentRowData;
    }
    // Hàm hiển thị đơn hàng lên modal Update status
    function showDataOrderToModal(paramResponse){
        "use strict";
        $('#inp-idorder-modal-update').val(paramResponse.orderId);
        $('#inp-combo-modal-update').val(paramResponse.kichCo);
        $('#inp-duong-kinh-modal-update').val(paramResponse.duongKinh);
        $('#inp-suon-nuong-modal-update').val(paramResponse.suon);
        $('#inp-drink-modal-update').val(paramResponse.idLoaiNuocUong);
        $('#inp-numberdrink-modal-update').val(paramResponse.soLuongNuoc);
        if(paramResponse.idVourcher == ""){
            $('#inp-voucherid-modal-update').val("-----");
        }
        else{
            $('#inp-voucherid-modal-update').val(paramResponse.idVourcher);
        }        
        $('#inp-typepizza-modal-update').val(paramResponse.loaiPizza);
        $('#inp-salad-modal-update').val(paramResponse.salad);
        $('#inp-price--modal-update').val(paramResponse.thanhTien + " VNĐ");
        $('#inp-giam-gia-modal-update').val(paramResponse.giamGia + " VNĐ");
        $('#inp-fullname-modal-update').val(paramResponse.hoTen);
        if(paramResponse.email == ""){
            $('#inp-email-modal-update').val("-----");
        }
        else{
            $('#inp-email-modal-update').val(paramResponse.email);
        }        
        $('#inp-mobile-modal-update').val(paramResponse.soDienThoai);
        $('#inp-address-modal-update').val(paramResponse.diaChi);
        if(paramResponse.loiNhan == ""){
            $('#inp-message-modal-update').val("-----");
        }
        else{
            $('#inp-message-modal-update').val(paramResponse.loiNhan);
        }        
        $('#inp-status-modal-update').val(paramResponse.trangThai);
    }
    // Hàm làm trắng modal add order
    function clearModalAddOrder(){
        "use strict";
        $('#input-combo-modal').val("");
        $('#input-duong-kinh-modal').val("");
        $('#input-suon-modal').val("");
        $('#input-salad-modal').val("");
        $('#input-soluongdrink-modal').val("");
        $('#input-money-modal').val("");
        $('#select-type-pizza-modal').val("none");
        $('#select-drink-modal').val("none");
        $('#input-hoten-modal').val("");
        $('#input-email-modal').val("");
        $('#input-phone-modal').val("");
        $('#input-address-modal').val("");
        $('#input-message-modal').val("");
        $('#input-idvoucher-modal').val("");
        $('#input-giam-gia-modal').val("");
    }

});